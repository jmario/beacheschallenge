'use strict'

import gulp         from 'gulp';
import connect      from 'gulp-connect';
import open         from 'gulp-open';
import sourcemaps   from 'gulp-sourcemaps';
import rename       from 'gulp-rename';
import buffer       from 'vinyl-buffer';
import source       from 'vinyl-source-stream';
import plumber      from 'gulp-plumber';
import merge        from 'utils-merge';
import chalk 		from 'chalk';
import notify		from 'gulp-notify';


/* ---------------- JS ---------------- */
import eslint       from 'gulp-eslint';
import uglify       from 'gulp-uglify';
import browserify   from 'browserify';
import watchify     from 'watchify';
import babelify     from 'babelify';


/* -------------- Styles -------------- */
import postcss    	from 'gulp-postcss';
import variables  	from 'postcss-simple-vars';
import nesting    	from 'postcss-nested';
import cssimports 	from 'postcss-import';
import mixins   	from 'postcss-mixins';
import autoprefixer from 'autoprefixer';
import cssnano 		from 'cssnano';

import paths from './app/conf/paths.conf.js';

const { dev, dist } = paths;
const port          = 3008;

function onError(error) {
	let report    = '';
	report 		  += (error.lineNumber) ? error.lineNumber : '';
	var _chalk = chalk.red;

    report += _chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += _chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) { report += _chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
    if (error.fileName)   { report += _chalk('FILE:') + ' ' + error.fileName + '\n'; }
    console.log(report);
	this.emit('end');
};

const jslint = (modifiedFiles) => {

    return gulp
        .src(modifiedFiles)
        .pipe(eslint({ useEslintrc: true }))
        .pipe(eslint.format());
};

const bundle_js = (bundler) => {
	return bundler.bundle()
	    .pipe(source(dev.scripts))
	    .pipe(buffer())
	    .pipe(gulp.dest(dist.scripts))
	    .pipe(rename('app.min.js'))
	    .pipe(sourcemaps.init({ loadMaps: true }))
	    .pipe(uglify())
	    .pipe(sourcemaps.write())
	    .pipe(gulp.dest(dist.scripts))
	    .pipe(connect.reload())
	    .pipe(notify("Scripts Loaded."));
}

gulp.task('scripts', () => {
	const args = merge(watchify.args, {debug:true});
  	let bundler = watchify(browserify(`${dev.scripts}/app.js`, args)).transform(babelify, { presets: ['es2015']});
 	bundle_js(bundler)	
	
  	bundler.on('update', (modifiedFiles) => {
  		jslint(modifiedFiles);
    	bundle_js(bundler);
  	})
});

gulp.task('styles', () => {
	const compatibility = { browsers: ['last 2 versions'] };
	const plugins       = [ cssimports, variables, nesting, autoprefixer(), cssnano() ];
	return gulp.src(`${dev.styles}/main.css`)
				.pipe(plumber(onError))
				.pipe(sourcemaps.init())
				.pipe(postcss(plugins))
				.pipe(sourcemaps.write())
				.pipe(rename('master.min.css'))
				.pipe(plumber.stop())
				.pipe(gulp.dest(`${dist.styles}`))
        		.pipe(connect.reload())
        		.pipe(notify("Styles Loaded."));
});

gulp.task('watch', ['connect'], () => {
	gulp.watch([`${dev.styles}/**/*.css`], ['styles']);
});

gulp.task('connect', () => {
	connect.server({
    	root: 'app',
    	port: port,
    	livereload: true,
    	fallback: `${dev.views}/index.html`
  	});
});

gulp.task('app', () => {
  	const options   =	{
					    	uri: `http://localhost:${port}`
					  	};

  	gulp.src(`${dev.views}/index.html`)
  		.pipe(open(options));
});

gulp.task('default', ['connect','styles','scripts', 'watch', 'app'] );


