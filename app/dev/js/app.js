import jQuery from 'jQuery';

(function x($) {
    $.fn.slider = function slider(options) {

        /* ------- CREATE SLIDER DOM ------- */     
        const $container       = $(this);
        const $dotsContainer   = $container.find('.nav-dots ul');
        const $slideContainer  = $container.find('.nav-images');

        const init = () => {
            for (const slide of options.sliderData) {
                const isActive = slide.active ? 'active' : '';
                const _slide = $('<img></img>').addClass(`slide ${isActive}`).attr('src', slide.location);
                const _dot = $('<li></li>').addClass(`nav-dot ${isActive}`);
                
                $slideContainer.append(_slide);
                $dotsContainer.append(_dot);
            }
        };

        init();

        /* ------- FUNCTIONALITY ------- */
        const $play            = $container.find('.play-pause');
        const $next            = $container.find('.next');
        const $prev            = $container.find('.prev');
        const $slides          = $slideContainer.find('.slide');
        const $navDots         = $dotsContainer.find('.nav-dot');
        let $activeDot         = $($navDots[0]);
        let $activeSlide       = $($slides[0]);
        let activeIndex        = 0;
        let isPlaying          = true;
        const slideCount       = $slides.length;

        const setActive = (newIndex) => {
            const $newActiveSlide   = $($slides[newIndex]);
            const $newActiveDot     = $($navDots[newIndex]);
            
            $activeSlide.removeClass('active');
            $activeDot.removeClass('active');
            $newActiveSlide.addClass('active');
            $newActiveDot.addClass('active');

            $activeDot = $newActiveDot;
            $activeSlide = $newActiveSlide;
            activeIndex = newIndex;
        };
    
        $next.on('click', () => {
            const nextIndex    = (activeIndex < slideCount - 1) ? activeIndex + 1 : 0;

            setActive(nextIndex);
        });

        $prev.on('click', () => {
            const prevIndex    = (activeIndex === 0) ? slideCount - 1 : activeIndex - 1;
            
            setActive(prevIndex);
        });

        $play.on('click', (e) => {
            const $this = $(e.currentTarget);

            isPlaying = !isPlaying;
            if (isPlaying) {
                $this.removeClass('playing');
            } else {
                $this.addClass('playing');
            }
        });
        
        $navDots.each( (index, navDot) => {
            $(navDot).on('click', (e) => {
                const $dot   = $(e.currentTarget);
                const $slide = $($slides[index]);

                $activeSlide.removeClass('active');
                $activeDot.removeClass('active');
                $(navDot).addClass('active');
                $slide.addClass('active');
                
                $activeDot   = $dot;
                $activeSlide = $slide;
                activeIndex  = index;
            });
        });

        window.setInterval( () =>{
            if(isPlaying) {

                $next.trigger('click');
            }
        }, 5000);

    };

})(jQuery);

const path = '/dist/images/slider';
const sliderData = [
    {
        location: `${path}/slide1.jpg`,
        active: true
    },
    {
        location: `${path}/slide2.jpg`
    },
    {
        location: `${path}/slide3.jpg`
    },
    {
        location: `${path}/slide4.jpg`
    },
    {
        location: `${path}/slide5.jpg`
    },
    {
        location: `${path}/slide6.jpg`
    },
    {
        location: `${path}/slide7.jpg`
    },
    {
        location: `${path}/slide8.jpg`
    },
    {
        location: `${path}/slide9.jpg`
    }
];

jQuery('#home-slider').slider({ sliderData: sliderData });