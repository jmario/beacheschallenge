const devroot = './app/dev';
const distroot = './app/dist';

module.exports = { 
	dev: {
		scripts: `${devroot}/js`,
		styles: `${devroot}/css`,
		images: `${devroot}/images`,
		views: `${devroot}/views`,
		root: `${devroot}`
	},
	dist: {
		scripts: `${distroot}/js`,
		styles: `${distroot}/css`,
		images: `${distroot}/images`,
		views: `${distroot}/views`,
		root: `${distroot}`
	}
};